(function($) {
  
  jQuery.fn.outerHTML = function(s) {
    return s
    ? this.before(s).remove()
    : jQuery("<p>").append(this.eq(0).clone()).html();
  };
  
  Drupal.behaviors.OgAudienceFieldExampleWidget = {
    attach: function() {
      $('.og-audience-field-example-widget').each(function(i, container) {
        var validDropdowns = 
          $(container).find('select[name="first"]').length &&
          $(container).find('select[name="second"]').length &&
          $(container).find('select[name="third"]').length;
        
        if (validDropdowns) {
          $first = $(container).find('select[name="first"]');
          $second = $(container).find('select[name="second"]');
          $third = $(container).find('select[name="third"]');
          // First dropdown
          $first.once('select-first', function() {
            var changeSecond = function(elem) {
              var origOpts = $second.data('opts') ? $second.data('opts') : false;
              if (!origOpts) {
                $second.data('opts', $second.outerHTML());
              }
              var toWrite = $($second.data('opts')).find('option[value^=' + $first.val() + ']');
              $second.html(toWrite);
              $second.trigger('changeThird');
            };
            $(this).bind('change', changeSecond);
          });
          // Second dropdown
          $second.once('select-second', function() {
            var changeThird = function(elem) {
              var origOpts = $third.data('opts') ? $third.data('opts') : false;
              if (!origOpts) {
                $third.data('opts', $third.outerHTML());
              }
              var toWrite = $($third.data('opts')).find('option[value^=' + $first.val() + '.' + $second.val() + ']');
              $third.html(toWrite);
            };
            $(this).bind('change', changeThird);
            $(this).bind('changeThird', changeThird);
          });
        }
      });
    }
  };
  
})(jQuery);