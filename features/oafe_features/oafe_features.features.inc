<?php
/**
 * @file
 * oafe_features.features.inc
 */

/**
 * Implements hook_node_info().
 */
function oafe_features_node_info() {
  $items = array(
    'group' => array(
      'name' => t('Group'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'group_content' => array(
      'name' => t('Group Content'),
      'base' => 'node_content',
      'description' => t('Content that can be shared with groups.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
