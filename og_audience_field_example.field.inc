<?php

/**
 * Implements hook_field_widget_info().
 */
function og_audience_field_example_field_widget_info() {
  return array(
    OG_AUDIENCE_EXAMPLE_WIDGET => array(
      'label' => t('Example Group audience'),
      'settings' => array(),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM
      ),
      'field types' => array('group')
    )
  );
}

/**
 * Implements hook_field_widget_form().
 */
function og_audience_field_example_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $base) {
  // @todo Support default value based on $items, $delta
  $element['wrapper'] = array(
    '#type' => 'container',
    '#prefix' => '<div class="og-audience-field-example-widget">',
    '#tree' => FALSE,
    '#suffix' => '</div>'
  );
  $element['wrapper']['first'] = array(
    '#type' => 'select',
    '#title' => t('Level one'),
    '#options' => array(
      'one' => t('One'),
      'two' => t('Two'),
      'three' => t('Three')
    )
  );
  $element['wrapper']['second'] = array(
    '#type' => 'select',
    '#title' => t('Level two'),
    '#options' => array(
      'one.one' => t('One'),
      'one.two' => t('Two'),
      'one.three' => t('Three'),
      'two.one' => t('Two'),
      'two.two' => t('Four'),
      'two.three' => t('Six'),
      'three.one' => t('Three'),
      'three.two' => t('Six'),
      'three.three' => t('Nine')
    )
  );
  $element['wrapper']['third'] = array(
    '#type' => 'select',
    '#title' => t('Level three'),
    '#options' => array(
      'one.one.one' => t('One'),
      'one.one.two' => t('Two'),
      'one.one.three' => t('Three'),
      'one.two.one' => t('Two'),
      'one.two.two' => t('Four'),
      'one.two.three' => t('Six'),
      'one.three.one' => t('Three'),
      'one.three.two' => t('Six'),
      'one.three.three' => t('Nine'),
      'two.one.one' => t('Two'),
      'two.one.two' => t('Four'),
      'two.one.three' => t('Six'),
      'two.two.one' => t('Four'),
      'two.two.two' => t('Eight'),
      'two.two.three' => t('Twelve'),
      'two.three.one' => t('Six'),
      'two.three.two' => t('Twelve'),
      'two.three.three' => t('Eighteen'),
      'three.one.one' => t('Three'),
      'three.one.two' => t('Six'),
      'three.one.three' => t('Nine'),
      'three.two.one' => t('Six'),
      'three.two.two' => t('Twelve'),
      'three.two.three' => t('Eighteen'),
      'three.three.one' => t('Nine'),
      'three.three.two' => t('Eighteen'),
      'three.three.three' => t('Twenty seven')
    )
  );
  $element['#attached']['js'][drupal_get_path('module', 'og_audience_field_example') . '/og_audience_field_example.js'] = array(
    'data' => drupal_get_path('module', 'og_audience_field_example') . '/og_audience_field_example.js',
    'type' => 'file'
  );
  $element['#element_validate'] = array('og_audience_field_example_field_widget_form_validate');
  $base += $element;
  return $base;
}

/**
 * Helper that converts a string to a number.
 * 
 * @param string $string
 *   String-version of a number.
 */
function _oafe_str_to_number($string) {
  switch ($string) {
    case 'one': return 1; break;
    case 'two': return 2; break;
    case 'three': return 3; break;
    default: throw new Exception();
  }
}

/**
 * Used as a array_reduce() callback.
 */
function _oafe_multiply($num1, $num2) {
  return ($num1 * $num2);
}

/**
 * #element_validate callback.
 */
function og_audience_field_example_field_widget_form_validate(&$element, &$form_state) {
  $value = $element['wrapper']['third']['#value'];
  // @todo Add a check here to ensure the value of the third dropdown was set only by setting expected
  // values in the first and the second dropdowns.
  $parts_str = explode('.', $value);
  $parts = array_map('_oafe_str_to_number', $parts_str);
  $product = array_reduce($parts, '_oafe_multiply');
  // @todo Support multi-value fields with multi-select
  $nid = $product;
  $node = node_load($nid);
  if (!$node) {
    form_error($element, t('Invalid node'));
    return;
  }
  if (!og_is_group_type('node', $node->type)) {
    form_error($element, t('Not a group type'));
    return;
  }
  $group = og_get_group('node', $node->nid);
  if (!$group) {
    form_error($element, t('Not a group'));
    return;
  }
  form_set_value($element, array(array('gid' => $group->gid)), $form_state);
}